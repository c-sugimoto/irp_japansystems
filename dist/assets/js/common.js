$(function() {
    $('.js_slider').slick({
        autoplay: true,
        arrows: false,
        dots: true,
        dotsClass: 'm_mainVisual_nav',
        pauseOnHover: false,
        infinite: true,
        speed: 500,
        fade: true,
    });
    
    lazyload();

    function topBtn() {
        $(window).on("scroll", function() {
            if (window.matchMedia( "(max-width: 768px)" ).matches) {
                if ($(this).scrollTop() > 100) {
                    $("#topBtn").addClass('is_show');
                } else {
                    $("#topBtn").removeClass('is_show');
                }
                scrollHeight = $(document).height(); //ドキュメントの高さ 
                scrollPosition = $(window).height() + $(window).scrollTop(); //現在地 
                footHeight = $("footer").innerHeight(); //footerの高さ（＝止めたい位置）
                if ( scrollHeight - scrollPosition  <= footHeight ) { //ドキュメントの高さと現在地の差がfooterの高さ以下になったら
                    $("#topBtn").css({
                        "position":"absolute", //pisitionをabsolute（親：wrapperからの絶対値）に変更
                    });
                } else { //それ以外の場合は
                    $("#topBtn").css({
                        "position":"fixed", //固定表示
                    });
                }
            } else {
                $("#topBtn").removeClass('is_show');
            }
        });
        $('#topBtn').click(function () { // #topBtnをクリックすると
            $('body,html').animate({ // いちばん上にanimateする
            scrollTop: 0 // 戻る位置
            }, 400); // 戻るスピード
            return false;
        });
        
    }
    $(window).resize(function() {
        topBtn();
    });
    topBtn();
});